package com.wipwebshop.backend;

import com.wipwebshop.backend.category.CategoryController;
import com.wipwebshop.backend.category.model.Category;
import com.wipwebshop.backend.category.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BackendApplicationTests {

    @Test
    void contextLoads() {
    }

}
