package com.wipwebshop.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.authentication.JwtTokenUtil;
import com.wipwebshop.backend.category.model.Category;
import com.wipwebshop.backend.category.service.CategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class CategoryTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AccountDetailsService accountDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Rollback
    @Test
    public void addValidCategory() throws Exception {

        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();

        Category newCategory = new Category();
        newCategory.setCategoryName("AutomatedTestCategory");
        newCategory.setActive(true);

        String jsonDish = mapper.writeValueAsString(newCategory);
        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);
        MvcResult mvcResult = this.mockMvc.perform(post("/category")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDish))
                .andExpect(status().isOk())
                .andReturn();

        int newCategoryId = Integer.parseInt(mvcResult.getResponse().getContentAsString());
        newCategory.setCategoryId(newCategoryId);

        mvcResult = this.mockMvc.perform(get("/category/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*]", hasSize(categoryService.loadAllCategories().size())))
                .andExpect(status().isOk())
                .andReturn();

        Category[] allCategories = mapper.readValue(mvcResult.getResponse().getContentAsString(), Category[].class);
        Category lastCategory = allCategories[allCategories.length - 1];

        assertEquals(lastCategory.getCategoryId(), newCategory.getCategoryId());
        assertEquals(lastCategory.getCategoryName(), newCategory.getCategoryName() );

    }

    @Rollback
    @Test
    public void addInvalidCategoryWithoutName() throws Exception {
        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();

        Category newCategory = new Category();
        newCategory.setActive(true);

        String jsonDish = mapper.writeValueAsString(newCategory);
        try {
            MvcResult mvcResult = this.mockMvc.perform(post("/category")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonDish))
                    .andReturn();
            assertThat(false);
        } catch (Exception ex) {
            assertThat(true);
        }

    }
}
