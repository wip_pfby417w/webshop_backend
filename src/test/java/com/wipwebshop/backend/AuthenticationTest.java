package com.wipwebshop.backend;

import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.authentication.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationTest {

    @Autowired
    AccountDetailsService accountDetailsService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Test
    public void shouldReturnUnauthorizedBadCredentials() throws Exception {
        String input = "{\"username\":\"Test\",\"password\":\"BadPwd\"}";

        this.mockMvc.perform(post("/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(input))
                .andExpect(status().isUnauthorized())
                .andExpect(content().string(""));
    }

    @Test
    public void shouldReturnValidToken() throws Exception {
        String input = "{\"username\":\"Jonas\",\"password\":\"Jonas\"}";
        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");

        MvcResult mvcResult = this.mockMvc.perform(post("/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .content(input))
                .andReturn();

        String body = mvcResult.getResponse().getContentAsString();
        String token = body.substring(10);
        token = token.substring(0, token.length() - 2);
        int status = mvcResult.getResponse().getStatus();

        assertThat(this.jwtTokenUtil.validateToken(token, userDetails));
        assertThat(status == 200);
    }

    @Test
    public void shouldReturnBadRequestNoCredentials() throws Exception {
        this.mockMvc.perform(post("/signin"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(""));
    }
}
