package com.wipwebshop.backend;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.wipwebshop.backend.account.model.Account;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.authentication.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AccountTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AccountDetailsService accountDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Rollback
    @Test
    @JsonInclude(value = JsonInclude.Include.NON_NULL, content = JsonInclude.Include.NON_NULL)
    public void addValidUser() throws Exception {

        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();
        Account newAccount = new Account();
        newAccount.setUsername("AutomatedTestAccount");
        newAccount.setPassword("Geheimes1Geheim2passwort3.");
        newAccount.setEmail("AutomatedTestAccount@Test.de");
        newAccount.setForename("AutomatedTestAccountForname");
        newAccount.setSurname("AutomatedTestAccountSurname");
        newAccount.setStreet("AutomatedTestAccountStreet");
        newAccount.setHousenumber("1");
        newAccount.setPostcode(33100);
        newAccount.setCity("Paderborn");
        newAccount.setPhone("052511234");
        newAccount.setGroupId(1);
        String jsonString = "{\"username\":\"AutomatedTestAccount\"," +
                "\"password\":\"Geheimes1Geheim2passwort3.\"," +
                "\"email\":\"AutomatedTestAccount@Test.de\"," +
                "\"forename\":\"AutomatedTestAccountForname\"," +
                "\"surname\":\"AutomatedTestAccountSurname\"," +
                "\"street\":\"AutomatedTestAccountStreet\"," +
                "\"housenumber\":\"1\"," +
                "\"postcode\":33100," +
                "\"city\":\"Paderborn\"," +
                "\"phone\":\"052511234\"," +
                "\"groupid\":1}";

        MvcResult mvcResult = this.mockMvc.perform(post("/user/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonString))
                .andExpect(status().isOk())
                .andReturn();


        List<Account> allAccounts = this.accountDetailsService.loadAllUser();
        Account lastAccount = allAccounts.get(allAccounts.size() - 1);

        assertEquals(lastAccount.getUsername(), newAccount.getUsername());
        assertEquals(lastAccount.getEmail(), newAccount.getEmail());
        assertEquals(lastAccount.getForename(), newAccount.getForename());
        assertEquals(lastAccount.getSurname(), newAccount.getSurname());
        assertEquals(lastAccount.getStreet(), newAccount.getStreet());
        assertEquals(lastAccount.getHousenumber(), newAccount.getHousenumber());
        assertEquals(lastAccount.getPostcode(), newAccount.getPostcode());
        assertEquals(lastAccount.getCity(), newAccount.getCity());
        assertEquals(lastAccount.getPhone(), newAccount.getPhone());

    }
    
}

