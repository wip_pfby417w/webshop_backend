package com.wipwebshop.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.authentication.JwtTokenUtil;
import com.wipwebshop.backend.category.service.CategoryService;
import com.wipwebshop.backend.dish.model.Dish;
import com.wipwebshop.backend.dish.service.DishService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class DishTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private DishService dishService;
    private final String db_host = "localhost";
    private final String db_port = "3306";
    private final String db_user = "root";
    private final String db_pass = "";
    private final String db_base = "backend_webshop";
    @Autowired
    private CategoryService categoryService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AccountDetailsService accountDetailsService;

    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private Connection connection = null;

    @Test
    public void createConnection() throws SQLException, ClassNotFoundException {
        //Treiber initialisieren
        Class.forName(DRIVER);
        //URL für die Verbindung zu der Datenbank
        String mySqlUrl = "jdbc:mysql://" + db_host + ":" + db_port + "/" + db_base;
        //Verbindung herstellen
        connection = DriverManager.getConnection(mySqlUrl, db_user, db_pass);
    }

    @Test
    public void shouldReturnAllDish() throws Exception {
        String[][] dishListDbArray = new String[20][5];

        createConnection();

        //Statement mit Benennung der Tabelle
        String query = "SELECT * FROM dishes";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);

        int i = 0;
        while (rs.next()){
            dishListDbArray[i][0] =  rs.getString("dishId");
            dishListDbArray[i][1] =  rs.getString("categoryId");
            dishListDbArray[i][2] =  rs.getString("dish");
            dishListDbArray[i][3] =  rs.getString("toppings");
            dishListDbArray[i][4] =  rs.getString("active");
            i++;
        }

        MvcResult mvcResult = this.mockMvc.perform(get("/dish/list")
                .contentType(MediaType.APPLICATION_JSON))
               .andExpect(jsonPath("$[*]", hasSize(dishService.loadActiveDishes().size())))
                .andExpect(status().isOk())
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString(StandardCharsets.UTF_8);

        for(int j=0; j < i; j++){
            //System.out.println(dishListDbArray[j][0]);
            String dishId = JsonPath.parse(response).read("$["+j+"].dishId").toString();
            assertEquals(dishId, dishListDbArray[j][0]);
            String categoryId = JsonPath.parse(response).read("$["+j+"].categoryId").toString();
            assertEquals(categoryId, dishListDbArray[j][1]);
            String dish = JsonPath.parse(response).read("$["+j+"].dish").toString();
            assertEquals(dish, dishListDbArray[j][2]);
            String toppings = JsonPath.parse(response).read("$["+j+"].toppings").toString();
            assertEquals(toppings, dishListDbArray[j][3]);
            Boolean activeB = JsonPath.parse(response).read("$["+j+"].active");
            Integer activeI;
            if (activeB == true) {
                activeI = 1;
            } else {
                activeI = 0;
            }
            activeI.toString();
            String activeS = activeI.toString();
            assertEquals(activeS, dishListDbArray[j][4]);
        }

        assertEquals(mvcResult.getResponse().getStatus(), 200);
    }

    @Rollback
    @Test
    public void addValidDish() throws Exception {

        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();


        int validCatId = this.categoryService.loadAllCategories().get(0).getCategoryId();
        String validPicture = this.dishService.loadActiveDishes().get(0).getPicture();


        Dish newDish = new Dish();
        newDish.setActive(true);
        newDish.setCategoryId(validCatId);
        newDish.setDish("AutomatedTestDish");
        newDish.setPicture(validPicture);
        newDish.setToppings("AutomatedTestToppings");

        String jsonDish = mapper.writeValueAsString(newDish);

        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);
        MvcResult mvcResult = this.mockMvc.perform(post("/dish")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDish))
                .andExpect(status().isOk())
                .andReturn();

        int newDishId = Integer.parseInt(mvcResult.getResponse().getContentAsString());
        newDish.setDishId(newDishId);
        mvcResult = this.mockMvc.perform(get("/dish/list")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*]", hasSize(dishService.loadActiveDishes().size())))
                .andExpect(status().isOk())
                .andReturn();

        Dish[] allDishes = mapper.readValue(mvcResult.getResponse().getContentAsString(), Dish[].class);
        Dish lastDish = allDishes[allDishes.length - 1];

        assertEquals(lastDish.getPicture(), newDish.getPicture());
        assertEquals(lastDish.getDish(), newDish.getDish());
        assertEquals(lastDish.getDishId(), newDish.getDishId());
        assertEquals(lastDish.getCategoryId(), newDish.getCategoryId());
        assertEquals(lastDish.getToppings(), newDish.getToppings());
        assertEquals(lastDish.getSizes(), newDish.getSizes());
    }

    @Rollback
    @Test
    public void addInvalidDishWithoutName() throws Exception {
        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();


        int validCatIt = this.categoryService.loadAllCategories().get(0).getCategoryId();
        String validPicture = this.dishService.loadActiveDishes().get(0).getPicture();


        Dish newDish = new Dish();
        newDish.setActive(true);
        newDish.setCategoryId(validCatIt);
        newDish.setPicture(validPicture);
        newDish.setToppings("AutomatedTestToppings");

        String jsonDish = mapper.writeValueAsString(newDish);
        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);
        try {
            MvcResult mvcResult = this.mockMvc.perform(post("/dish")
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonDish))
                    .andReturn();
            assertThat(false);
        } catch (Exception ex) {
            assertThat(true);
        }

    }

    @Rollback
    @Test
    public void editValidDish() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        Dish validDish = this.dishService.loadAllDishes().get(0);

        validDish.setDish("Automated Testing");
        validDish.setActive(false);

        String jsonDish = mapper.writeValueAsString(validDish);
        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);
        MvcResult mvcResult = this.mockMvc.perform(put("/dish")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDish))
                .andExpect(status().isOk())
                .andReturn();

        Dish editedDish = this.dishService.loadDishById(validDish.getDishId());
        assertThat(validDish.getDish() == editedDish.getDish());
        assertThat(validDish.isActive() == editedDish.isActive());

    }


    @Rollback
    @Test
    public void editInvalidDish() throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        List<Dish> allDishes = this.dishService.loadAllDishes();

        Dish lastDish = allDishes.get(allDishes.size() - 1);
        Dish invalidDish = new Dish();
        invalidDish.setDishId(invalidDish.getDishId() + 1);
        invalidDish.setCategoryId(lastDish.getCategoryId());
        invalidDish.setPicture(lastDish.getPicture());
        invalidDish.setPictureId(lastDish.getPictureId());
        invalidDish.setToppings(lastDish.getToppings());
        invalidDish.setDish("Automated Testing");
        invalidDish.setActive(false);

        String jsonDish = mapper.writeValueAsString(invalidDish);
        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);
        try {
            MvcResult mvcResult = this.mockMvc.perform(put("/dish")
                    .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonDish))
                    .andExpect(status().is(404))
                    .andReturn();
            assertThat(false);
        } catch (Exception ex) {

            assertThat(ex.getCause().getMessage().contains("404"));
        }


    }
}
