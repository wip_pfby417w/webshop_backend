package com.wipwebshop.backend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wipwebshop.backend.account.model.Address;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.account.service.AddressService;
import com.wipwebshop.backend.authentication.JwtTokenUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class AddressTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private AddressService addressService;
    @Autowired
    private AccountDetailsService accountDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Rollback
    @Test
    public void addValidAddress() throws Exception {

        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();

        Address newAddress = new Address();
        newAddress.setForename("AutomatedTestForename");
        newAddress.setSurname("AutomatedTestSurename");
        newAddress.setStreet("AutomatedTestStreet");
        newAddress.setHousenumber("1");
        newAddress.setPostcode("33102");
        newAddress.setCity("AutomatedTestCity");
        String jsonString = "{\"forename\":\"AutomatedTestForename\"," +
                "\"surname\":\"AutomatedTestSurename\"," +
                "\"street\":\"AutomatedTestStreet\"," +
                "\"housenumber\":\"1\"," +
                "\"postcode\":33102," +
                "\"city\":\"AutomatedTestCity\"}";

        UserDetails userDetails = this.accountDetailsService.loadUserByUsername("Jonas");
        String token = jwtTokenUtil.generateToken(userDetails);

        MvcResult mvcResult = this.mockMvc.perform(post("/address")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + token)
                .content(jsonString))
                .andExpect(status().isOk())
                .andReturn();

        int newAddressId = Integer.parseInt(mvcResult.getResponse().getContentAsString());

        Address lastAddress = this.addressService.findById(newAddressId);

        assertEquals(lastAddress.getForename(), newAddress.getForename());
        assertEquals(lastAddress.getSurname(), newAddress.getSurname());
        assertEquals(lastAddress.getStreet(), newAddress.getStreet());
        assertEquals(lastAddress.getHousenumber(), newAddress.getHousenumber());
        assertEquals(lastAddress.getPostcode(), newAddress.getPostcode());
        assertEquals(lastAddress.getCity(), newAddress.getCity());
    }

    @Rollback
    @Test
    public void addInvalidAddressWithoutCity() throws Exception {
        //Creating the ObjectMapper object
        ObjectMapper mapper = new ObjectMapper();

        Address newAddress = new Address();
        newAddress.setForename("AutomatedTestForename");
        newAddress.setSurname("AutomatedTestSurename");
        newAddress.setStreet("AutomatedTestStreet");
        newAddress.setHousenumber("1");
        newAddress.setPostcode("33102");

        String jsonAddress = mapper.writeValueAsString(newAddress);
        try {
            MvcResult mvcResult = this.mockMvc.perform(post("/address")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonAddress))
                    .andReturn();
            assertThat(false);
        } catch (Exception ex) {
            assertThat(true);
        }

    }
}
