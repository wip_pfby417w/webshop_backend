package com.wipwebshop.backend.exceptions;

/**
 * Thrown if a new user already exists
 *
 * @author Rene Schiffner
 */
public class UserAlreadyExistException extends RuntimeException {
    public UserAlreadyExistException(final String message) {
        super(message);
    }
}
