package com.wipwebshop.backend.exceptions;

/**
 * Thrown if an item already exists in database
 *
 * @author Rene Schiffner
 */
public class ItemAlreadyExistsException extends RuntimeException {
    public ItemAlreadyExistsException(final String message) {
        super(message);
    }
}
