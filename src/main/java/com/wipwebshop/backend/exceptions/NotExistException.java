package com.wipwebshop.backend.exceptions;

/**
 * Thrown if an item does no exist
 *
 * @author Rene Schiffner
 */
public class NotExistException extends RuntimeException {
    public NotExistException(final String message) {
        super(message);
    }
}


