package com.wipwebshop.backend.pictures.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Model of a stored picture
 *
 * @author Jonas mertens
 */
@Entity
@Table(name = "pictures")
public class Picture {
    @Id
    @Column(name = "pictureid", columnDefinition = "BINARY(16)")
    private UUID pictureId;

    @Column(name = "picture")
    private String picture;

    public Picture() {
    }

    public Picture(UUID pictureId, String picture) {
        this.pictureId = pictureId;
        this.picture = picture;
    }

    /**
     * Returns the stringified picture
     *
     * @return stringified picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Sets the picture data with stringified picture
     *
     * @param picture stringified picture to set
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * Returns the uuid of the picture
     *
     * @return pictures uuid
     */
    public UUID getPictureId() {
        return pictureId;
    }

    /**
     * Sets the picture uuid
     *
     * @param pictureId uuid to set
     */
    public void setPictureId(UUID pictureId) {
        this.pictureId = pictureId;
    }
}

