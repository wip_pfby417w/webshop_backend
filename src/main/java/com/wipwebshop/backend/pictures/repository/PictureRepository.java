package com.wipwebshop.backend.pictures.repository;

import com.wipwebshop.backend.pictures.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Repository Class to access picture data in database. Methods such as save, select are available as default
 *
 * @author Jonas Mertens
 */
public interface PictureRepository extends JpaRepository<Picture, String> {
    /**
     * Finds Picture by its is
     *
     * @param pictureId pictureId to look for
     * @return found picture
     */
    Picture findByPictureId(UUID pictureId);
}
