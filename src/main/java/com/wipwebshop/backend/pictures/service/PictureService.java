package com.wipwebshop.backend.pictures.service;

import com.wipwebshop.backend.pictures.model.Picture;
import com.wipwebshop.backend.pictures.repository.PictureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * <p>
 * This Serviceclass implements methods belonging to pictures.
 * </p>
 *
 * @author Jonas Mertens
 */
@Service
public class PictureService {

    @Autowired
    PictureRepository pictureRepository;

    /**
     * saves a new picture to database
     *
     * @param pictureAsString stringified picture data
     * @return id of saved picture
     */
    public UUID savePicture(String pictureAsString) {
        UUID pictureId = UUID.randomUUID();
        Picture picture = new Picture(pictureId, pictureAsString);
        pictureRepository.save(picture);
        return pictureId;
    }

    /**
     * Gets picture from database, found by id
     *
     * @param pictureId pictureId to look for
     * @return found picture
     */
    public Picture getPicture(UUID pictureId) {
        Picture picture = pictureRepository.findByPictureId(pictureId);
        return picture;
    }
}
