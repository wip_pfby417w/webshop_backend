package com.wipwebshop.backend.pictures;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for pictures
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class PictureController {

}
