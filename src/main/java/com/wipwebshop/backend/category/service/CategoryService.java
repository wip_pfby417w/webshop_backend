package com.wipwebshop.backend.category.service;

import com.wipwebshop.backend.category.model.Category;
import com.wipwebshop.backend.category.repository.CategoryRepository;
import com.wipwebshop.backend.exceptions.ItemAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * This Serviceclass implements methods beloging to categories.
 * </p>
 *
 * @author Jonas Mertens, Rene Schiffner
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;


    /**
     * Service for listing all Categories, whether active or inactive
     *
     * @return List of all Categories
     */
    public <Iterable> List<Category> loadAllCategories() {
        return categoryRepository.findAll();
    }

    /**
     * Adds a new category
     *
     * @param category new Category Object, not null
     * @return ID for new category
     */
    public int addCategory(Category category) {
        // check if category already exists
        if (loadCategoryByName(category.getCategoryName()) != null) {
            throw new ItemAlreadyExistsException("There is an category with that name:" + category.getCategoryName());
        }

        categoryRepository.save(category);
        return loadCategoryByName(category.getCategoryName()).getCategoryId();
    }

    /**
     * Loads a category find by name
     *
     * @param categoryName Name to look for, not null
     * @return Found CategoryObject, null if no category found
     */
    public Category loadCategoryByName(String categoryName) {
        return categoryRepository.findByCategoryName(categoryName);
    }
}
