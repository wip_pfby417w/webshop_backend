package com.wipwebshop.backend.category;

import com.wipwebshop.backend.category.model.Category;
import com.wipwebshop.backend.category.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * ControllerClass for HTTP controllers belonging to categories
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@RestController
@Service
@CrossOrigin  //TODO beheben
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    // Controller for listing all Categories, whether active or inactive
    @GetMapping(path = "/category/list")
    public @ResponseBody
    Iterable<Category> listAllCategories() {
        return categoryService.loadAllCategories();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/category")
    public int addCategory(@RequestBody Category category) throws HttpClientErrorException.UnprocessableEntity {
        if (category == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        return categoryService.addCategory(category);
    }

}
