package com.wipwebshop.backend.category.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Implements categories for dishes
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "categories")
public class Category {

    // primary key in table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "categoryid")
    private int categoryId;

    @NotNull
    @Column(name = "categoryname")
    private String categoryName;

    @NotNull
    @Column(name = "active")
    private boolean active;

    /**
     * Creates an empty category
     */
    public Category() {
    }

    /**
     * Creates an totaly filled category
     *
     * @param categoryName The Name of the new category
     * @param active       Is category active or not
     */
    public Category(String categoryName, boolean active) {
        this.categoryName = categoryName;
        this.active = active;
    }

    /**
     * Returns categoryId without any inspection
     *
     * @return categoryID
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Sets CategoryId without any inspection
     *
     * @param categoryId categoryId to set
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Returns CategoryName without any inspection
     *
     * @return CateogryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets CategoryName without any inspection
     *
     * @param categoryName CategoryName to set
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Returns active state without any inspection
     *
     * @return whether category is active or not
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets active state without any inspection
     *
     * @param active ActiveState to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

}
