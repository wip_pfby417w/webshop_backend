package com.wipwebshop.backend.category.repository;

import com.wipwebshop.backend.category.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository Class to access category data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

    Category findByCategoryName(String categoryName);
}
