package com.wipwebshop.backend.dish.service;

import com.wipwebshop.backend.dish.model.Dish;
import com.wipwebshop.backend.dish.model.Size;
import com.wipwebshop.backend.dish.repository.SizeRepository;
import com.wipwebshop.backend.exceptions.ItemAlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

/**
 * <p>
 * This Serviceclass implements methods belonging to sizes.
 * </p>
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Service
public class SizeService {

    @Autowired
    SizeRepository sizeRepository;

    /**
     * Loads one size by its name and the associated dish
     *
     * @param dish associated dish
     * @param name name of size
     * @return Size Object, null if no size found
     */
    public Size loadSizeByDishAndName(Dish dish, String name) {
        return loadSizeByDishIdAndName(dish.getDishId(), name);
    }

    /**
     * Loads one size by its name and the associated dishId
     *
     * @param dishId ID of the associated Dish
     * @param name   name of size
     * @return Size Object, null if no size found
     */
    public Size loadSizeByDishIdAndName(int dishId, String name) {
        return sizeRepository.finidByDishIdAndName(dishId, name);
    }

    /**
     * Loads one size by its ID
     *
     * @param sizeId sizeId to load
     * @return Size Object, null if no Size found
     */
    public Size loadSizeById(int sizeId) {
        return sizeRepository.findBySizeId(sizeId);
    }

    /**
     * Adds a new Size to database if it does not already exist
     *
     * @param size Size Object to add
     */
    public void addSize(Size size) {
        // check if username exists
        if (loadSizeByDishIdAndName(size.getDishId(), size.getSize()) != null) {
            throw new ItemAlreadyExistsException("There is an size with that dish and size");
        }

        sizeRepository.save(size);
    }

    /**
     * edits an existing Size
     *
     * @param size Size to edit, has to contain valid sizeID
     */
    public void editDish(Size size) {
        if (loadSizeById(size.getSizeId()) == null) {
            throw HttpClientErrorException.NotFound.create(HttpStatus.NOT_FOUND, "Item with this ID nor found", null, null, null);
        }

        Size savedSize = loadSizeById(size.getSizeId());
        savedSize.setDishId(size.getDishId());
        savedSize.setPrice(size.getPrice());
        savedSize.setSize(size.getSize());
        sizeRepository.save(savedSize);
    }

    /**
     * Delets size by setting deletion state
     *
     * @param size size to mark es deleted
     */
    public void deleteSize(Size size) {
        size.setDeleted(true);
        sizeRepository.save(size);
    }
}
