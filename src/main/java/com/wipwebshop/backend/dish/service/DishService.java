package com.wipwebshop.backend.dish.service;

import com.wipwebshop.backend.dish.model.Dish;
import com.wipwebshop.backend.dish.repository.DishRepository;
import com.wipwebshop.backend.exceptions.ItemAlreadyExistsException;
import com.wipwebshop.backend.pictures.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

/**
 * <p>
 * This Serviceclass implements methods belonging to dishes.
 * </p>
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Service
public class DishService {

    @Autowired
    DishRepository dishRepository;

    @Autowired
    PictureService pictureService;

    /**
     * Gets list of dishes and adds picture to every dish
     *
     * @return List of dishes
     */
    public <Iterable> List<Dish> loadAllDishes() {
        List<Dish> allDishes = dishRepository.findAll();
        allDishes.forEach(dish -> dish = addPicture(dish));
        return allDishes;
    }

    /**
     * Gets list of active dishes and adds picture to every active dish
     *
     * @return List of active dishes
     */
    public <Iterable> List<Dish> loadActiveDishes() {
        List<Dish> activeDishes = dishRepository.findActive();
        activeDishes.forEach(dish -> dish = addPicture(dish));
        return activeDishes;
    }

    /**
     * Returns dish found by ID and adds the associated picture
     *
     * @param dishId dishId to look for
     * @return one dish, if found. Null if no dish was found
     */
    public Dish loadDishById(int dishId) {
        return addPicture(dishRepository.findById(dishId));
    }

    /**
     * Returns dish found by name and adds the associated picture
     *
     * @param dishName dishName to look for
     * @return one dish, if found. Null if no dish was found
     */
    public Dish loadDishByName(String dishName) {
        return addPicture(dishRepository.findByDish(dishName));
    }

    /**
     * Checks if input is an valid dish and adds it to database.
     *
     * @param dish valid dish object
     * @return ID of created dish
     */
    public int addDish(Dish dish) {
        // check if dish exists
        if (loadDishByName(dish.getDish()) != null) {
            throw new ItemAlreadyExistsException("There is an dish with that name:" + dish.getDish());
        }
        if (dish.getPicture() != "" && dish.getPicture() != null) {
            dish.setPictureId(pictureService.savePicture(dish.getPicture()));
        }
        dishRepository.save(dish);
        return dishRepository.findByDish(dish.getDish()).getDishId();
    }

    /**
     * Adds picture to existing dishObject for correctly returning dishObject. DishObject from database does not contain picture by default.
     *
     * @param dish dish to add picture.
     * @return dish with picture
     */
    private Dish addPicture(Dish dish) {
        if (dish == null) {
            return dish;
        }
        // check if uuid is initial
        if (!dish.getPictureId().toString().equals("00000000-0000-0000-0000-000000000000")) {
            dish.setPicture(pictureService.getPicture(dish.getPictureId()).getPicture());
        }
        return dish;

    }

    /**
     * edits an existing dish
     *
     * @param dish dish to edit. Has to contain valid dishId.
     */
    public void editDish(Dish dish) {
        if (loadDishById(dish.getDishId()) == null) {
            throw HttpClientErrorException.NotFound.create(HttpStatus.NOT_FOUND, "Item with this ID nor found", null, null, null);
        }
        if (dish.getPicture() != "" && dish.getPicture() != null) {
            dish.setPictureId(pictureService.savePicture(dish.getPicture()));
        }
        Dish savedDish = loadDishById(dish.getDishId());
        savedDish.setDish(dish.getDish());
        savedDish.setPictureId(dish.getPictureId());
        savedDish.setActive(dish.isActive());
        savedDish.setCategoryId(dish.getCategoryId());
        savedDish.setToppings(dish.getToppings());
        dishRepository.save(savedDish);
    }
}
