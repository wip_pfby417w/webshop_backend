package com.wipwebshop.backend.dish.repository;

import com.wipwebshop.backend.dish.model.Size;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository Class to access config size in database. Methods such as save, select are available as default
 *
 * @author Jonas Mertens
 */
@Repository
public interface SizeRepository extends JpaRepository<Size, Integer> {


    @Query(value = "SELECT * FROM sizes WHERE dishid =:dishId AND size=:name ", nativeQuery = true)
    Size finidByDishIdAndName(@Param("dishId") int dishId, @Param("name") String name);

    Size findBySizeId(@Param("sizeId") int sizeId);
}
