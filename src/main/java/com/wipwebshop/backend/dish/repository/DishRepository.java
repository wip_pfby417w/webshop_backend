package com.wipwebshop.backend.dish.repository;

import com.wipwebshop.backend.dish.model.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository Class to access dish data in database. Methods such as save, select are available as default
 *
 * @author Jonas Mertens
 */
@Repository
public interface DishRepository extends JpaRepository<Dish, Integer> {

    //@Query(value = "SELECT * FROM dishes WHERE dishId =:dishId", nativeQuery = true)
    // @Param("dishId")
    Dish findById(int dishId);

    Dish findByDish(String dishName);

    @Query(value = "SELECT * FROM dishes WHERE active", nativeQuery = true)
    <Iterable> List<Dish> findActive();


}


