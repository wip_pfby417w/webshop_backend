package com.wipwebshop.backend.dish;

import com.wipwebshop.backend.dish.model.Dish;
import com.wipwebshop.backend.dish.service.DishService;
import com.wipwebshop.backend.pictures.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Controllerclass for HTTP controllers belonging to dishes
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class DishController {

    @Autowired
    DishService dishService;

    @Autowired
    PictureService pictureService;

    /**
     * Lists all Dishes
     *
     * @return List of Dishes
     */
    @GetMapping(path = "/dish/list")
    public @ResponseBody
    Iterable<Dish> listAllDishes() {
        return dishService.loadAllDishes();
    }

    /**
     * List dishes, with status active
     *
     * @return List of active dishes
     */
    @GetMapping(path = "/dish/listActiveDishes")
    public @ResponseBody
    Iterable<Dish> listActiveDishes() {
        return dishService.loadActiveDishes();
    }

    /**
     * Searches for one dish by its id
     *
     * @param dishId dishId to look for
     * @return Dish found by id; Null if no dish was found
     */
    @GetMapping(path = "/dish/{dishId}")
    public @ResponseBody
    Dish loadSelectedDish(@PathVariable("dishId") int dishId) {
        return dishService.loadDishById(dishId);
    }

    /**
     * adds a new dish
     *
     * @param dish new Dish Object to save
     * @return ID of new Dish
     * @throws HttpClientErrorException.UnprocessableEntity Thrown if dish is null
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/dish")
    public int addDish(@RequestBody Dish dish) throws HttpClientErrorException.UnprocessableEntity {
        if (dish == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        return dishService.addDish(dish);
    }

    /**
     * Changes an existing dish
     *
     * @param dish dish to change. Has to contain valid dishId
     * @return Status OK if changed succesfull
     * @throws HttpClientErrorException.UnprocessableEntity Thrown if dish is null
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/dish")
    public HttpStatus editDish(@RequestBody Dish dish) throws HttpClientErrorException.UnprocessableEntity {
        if (dish == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        dishService.editDish(dish);
        return HttpStatus.OK;
    }


}
