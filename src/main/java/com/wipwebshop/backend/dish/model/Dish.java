package com.wipwebshop.backend.dish.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Modell of an dish item.
 *
 * @author Jonas Mertens
 */
@Entity
@Table(name = "dishes")
public class Dish {

    // primary key in table dish
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dishid")
        private int dishId;

    @Column(name = "categoryid")
        private int categoryId;

    @NotNull
    @Column(name = "dish")
    private String dish;

    @Column(name = "toppings")
    private String toppings;

    @Column(name = "active")
    @NotNull
    private boolean active;

    @Column(name = "pictureid")
    private UUID pictureId;

    @Transient
    private String picture;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "dishid")
    private List<Size> sizes = new ArrayList<>();

    /**
     * Returns dishId without any inspection
     *
     * @return dishId
     */
    public int getDishId() {
        return dishId;
    }

    /**
     * Sets dishId without any inspection
     *
     * @param dishId dishId to set
     */
    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    /**
     * Returns CategoryId without any inspection
     *
     * @return categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Sets categoryId without any inspection
     *
     * @param categoryId categoryId to set
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Returns dish name without any inspectoin
     *
     * @return dishName
     */
    public String getDish() {
        return dish;
    }

    /**
     * sets dishname without any inspection
     *
     * @param dish dishname to set
     */
    public void setDish(String dish) {
        this.dish = dish;
    }

    /**
     * returns list of all sizes without any inspectoin
     *
     * @return all Sizes for this dish
     */
    public List<Size> getSizes() {
        return sizes;
    }

    /**
     * Sets Sizes for dish
     *
     * @param sizes sizes to set
     */
    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    /**
     * Returns Topping String without any inspection
     *
     * @return Toppings
     */
    public String getToppings() {
        return toppings;
    }

    /**
     * Sets Topping String without any inspection
     *
     * @param toppings toppingsString to set
     */
    public void setToppings(String toppings) {
        this.toppings = toppings;
    }

    /**
     * Returns active state without any inspection
     *
     * @return whether dish is active or not
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets active state without any inspection
     *
     * @param active active state to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Returns picture id of the dish's picture
     *
     * @return pictureId
     */
    public UUID getPictureId() {
        return pictureId;
    }

    /**
     * Sets picture id of the dish's picture
     *
     * @param pictureId pictureId to set
     */
    public void setPictureId(UUID pictureId) {
        this.pictureId = pictureId;
    }

    /**
     * Returns picture of the dish
     *
     * @return picture of the dish
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Sets picture of dish object
     *
     * @param picture picture to set
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }


}
