package com.wipwebshop.backend.dish.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Model of an size of one dish
 *
 * @author Jonas Mertens
 */
@Entity
@Where(clause = "deleted='false'")
@Table(name = "sizes")
public class Size {

    // primary key in table size
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sizeid")
    private int sizeId;

    @NotNull
    @Column(name = "dishid")
    private int dishId;

    @NotNull
    @Column(name = "size")
    private String size;

    @NotNull
    @Column(name = "price")
    private double price;

    @Column(name = "deleted")
    private boolean deleted;

    /**
     * returns sizeId without any inspection
     *
     * @return unique sizeID of size
     */
    public int getSizeId() {
        return sizeId;
    }

    /**
     * sets sizeId without any inspection
     *
     * @param sizeId new SizeId
     */
    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    /**
     * returns sizeName without any inspection
     *
     * @return Name of Size
     */
    public String getSize() {
        return size;
    }

    /**
     * sets sizeName without any inspection
     *
     * @param size New name for the size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * returns the price of the size without any inspection
     *
     * @return price of the size
     */
    public double getPrice() {
        return price;
    }

    /**
     * sets the price of the size without any inspection
     *
     * @param price new price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * returns the dishId of the associated dish
     *
     * @return dishId
     */
    public int getDishId() {
        return dishId;
    }

    /**
     * sets the dishId of the associated dish
     *
     * @param dishId ID of dish to associate
     */
    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    /**
     * returns whather size is deleted or not
     *
     * @return true if deleted, false if not deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * sets deletion state without any inspection
     *
     * @param deleted whether size is deleted or not
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
