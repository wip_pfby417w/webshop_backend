package com.wipwebshop.backend.dish;


import com.wipwebshop.backend.dish.model.Size;
import com.wipwebshop.backend.dish.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Controllerclass for HTTP controllers belonging to sizes
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class SizeController {

    @Autowired
    SizeService sizeService;

    /**
     * Adds a new size to an existing dish
     * @param size Size to add, has to contain valid DishID
     * @throws HttpClientErrorException.UnprocessableEntity Thrown if size is invalid
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/size")
    public void addDish(@RequestBody Size size) throws HttpClientErrorException.UnprocessableEntity {
        if (size.getDishId() == 0 || size.getPrice() == 0 || size.getSize() == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        sizeService.addSize(size);
    }

    /**
     * Edits an existing size
     * @param size Edited size. Has to contain existing sieId
     * @return OK if successfull edited
     * @throws HttpClientErrorException.UnprocessableEntity Thrown if given size is invalid
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/size")
    public HttpStatus editSize(@RequestBody Size size) throws HttpClientErrorException.UnprocessableEntity {
        if (size == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        sizeService.editDish(size);
        return HttpStatus.OK;
    }

    /**
     * Marks an existing size as deleted
     * @param size size to delete, has to contain valid SizeID
     * @return OK, if successfull deleted
     * @throws HttpClientErrorException.UnprocessableEntity Thrown if size is invalid
     */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(path = "/size")
    public HttpStatus deleteSize(@RequestBody Size size) throws HttpClientErrorException.UnprocessableEntity {
        if (size == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        sizeService.deleteSize(size);
        return HttpStatus.OK;

    }


}
