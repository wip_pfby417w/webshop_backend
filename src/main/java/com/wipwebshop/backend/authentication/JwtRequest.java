package com.wipwebshop.backend.authentication;

import java.io.Serializable;

/**
 * Modell Class of JwtRequests. Stores received credentials
 *
 * @author Jonas Mertens
 */
public class JwtRequest implements Serializable {

    private String username;
    private String password;

    /**
     * Creates empty JwtRequest
     */
    public JwtRequest() {
    }

    /**
     * Creates full JwtRequest
     *
     * @param username username for request
     * @param passwort password for request
     */
    public JwtRequest(String username, String passwort) {
        this.setUsername(username);
        this.setPassword(password);
    }

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * Sets @param without any inspection
     *
     * @param username username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Sets @param without any inspection
     *
     * @param password password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
