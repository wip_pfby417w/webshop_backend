package com.wipwebshop.backend.authentication;

import java.io.Serializable;

/**
 * Helper Class for returning JwtToken
 *
 * @author Jonas Mertens
 */
public class JwtResponse implements Serializable {

    private final String jwttoken;

    /**
     * Creates Response with token
     *
     * @param token given token for response
     */
    public JwtResponse(String token) {
        this.jwttoken = token;
    }

    /**
     * returns @return without any inspection
     *
     * @return token from Response
     */
    public String getToken() {
        return this.jwttoken;
    }
}
