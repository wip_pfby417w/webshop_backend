package com.wipwebshop.backend.account;

import com.wipwebshop.backend.account.model.Address;
import com.wipwebshop.backend.account.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * Controllerclass for methods belonging to addresses.
 * </p>
 *
 * @author Jonas Mertens
 */
@RestController
@Service
public class AddressController {

    @Autowired
    AddressService addressService;

    /**
     * Controller for getting addressId for new or existing address.
     *
     * @param address The new or existing address.
     * @return AddressId for new or existing address.
     */
    @PostMapping(path = "/address")
    public int addAddress(@RequestBody Address address) {
        return addressService.addAddress(address);
    }


}
