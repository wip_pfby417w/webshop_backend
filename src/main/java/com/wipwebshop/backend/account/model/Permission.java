package com.wipwebshop.backend.account.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * Permission object can be assigned to accounts or groups
 * </p>
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "permissions")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "permissionid")
    private int permissionId;

    @NotNull
    private String name;

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public int getPermissionId() {
        return permissionId;
    }

    /**
     * Sets @param without any inspection
     *
     * @param permissionId
     */
    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets @param without any inspection
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}
