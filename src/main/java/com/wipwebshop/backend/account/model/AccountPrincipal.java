package com.wipwebshop.backend.account.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.wipwebshop.backend.Constants.ROLE_PREFIX;

/**
 * Implementation of UserDetails for permission management
 *
 * @author Jonas Mertens
 */
public class AccountPrincipal implements UserDetails {

    private final Account account;

    public AccountPrincipal(Account account){
        this.account = account;
    }

    @Override
    public String getUsername() {
        return account.getUsername();
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

        account.getPermissions().forEach(permission -> list.add(new SimpleGrantedAuthority(ROLE_PREFIX + permission.getName())));

        return list;
    }

}
