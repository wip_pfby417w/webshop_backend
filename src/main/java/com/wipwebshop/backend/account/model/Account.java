package com.wipwebshop.backend.account.model;

import org.springframework.lang.Nullable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.wipwebshop.backend.Constants.ROLE_PREFIX;


/**
 * <p>
 * Account is the technical implementation of a real person.
 * </p>
 * <p>
 * Each person is discribed by username, groups, address and has specific permission.
 * accountId is the technical id stored in database.
 * </p>
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "account")
public class Account implements UserDetails {
    // primary key in table account
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "accountid")
    private int accountId;

    // get group for account
    @OneToOne
    @JoinColumn(name = "groupid", insertable = false, updatable = false)
    Group_Permissions accountGroup;
    @NotNull
    private String username;

    @Nullable
    private String email;
    private String forename;
    private String surname;
    private String street;
    private String housenumber;
    private int postcode;
    private String password;
    private String city;
    private String phone;
    @Column(name = "groupid")
    private int groupId;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "account_permissions", joinColumns = {
            @JoinColumn(name = "accountid")}, inverseJoinColumns = {
            @JoinColumn(name = "permissionid")})
    private Set<Permission> permissions;


    /**
     * Creates an empty account.
     */
    public Account() {
    }

    /**
     * Creates a totaly filled account. Id is generated, when the account is written to database.
     *
     * @param username
     * @param password
     * @param email
     * @param forename
     * @param surname
     * @param street
     * @param housenumber
     * @param postcode
     * @param city
     * @param phone
     * @param groupId
     */
    public Account(String username, String password, String email, String forename, String surname, String street,
                   String housenumber, int postcode, String city, String phone, int groupId) {

        this.username = username;
        this.password = password;
        this.email = email;
        this.forename = forename;
        this.surname = surname;
        this.street = street;
        this.housenumber = housenumber;
        this.postcode = postcode;
        this.city = city;
        this.phone = phone;
        this.groupId = groupId;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();

        permissions.forEach(permission -> list.add(new SimpleGrantedAuthority(ROLE_PREFIX + permission.getName())));

        return list;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }


    /**
     * Returns @return without any inspection
     *
     * @return accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Returns @return without any inspection
     *
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns @return without any inspection
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }


    /**
     * Returns @return without any inspection
     *
     * @return E-Mail Address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets param without any inspection
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Returns @return without any inspection
     *
     * @return forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Sets param without any inspection
     *
     * @param forename
     */
    public void setForename(String forename) {
        this.forename = forename;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets param without any inspection
     *
     * @param surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets param without any inspection
     *
     * @param street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Housenumber
     */
    public String getHousenumber() {
        return housenumber;
    }

    /**
     * Sets param without any inspection
     *
     * @param housenumber
     */
    public void setHousenumber(String housenumber) {
        this.housenumber = housenumber;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Postcode
     */
    public int getPostcode() {
        return postcode;
    }

    /**
     * Sets param without any inspection
     *
     * @param postcode
     */
    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    /**
     * Returns @return without any inspection
     *
     * @return City
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets param without any inspection
     *
     * @param city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets param without any inspection
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Returns @return without any inspection
     *
     * @return groupId
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * Sets param without any inspection
     *
     * @param groupId
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    /**
     * Returns @return without any inspection
     *
     * @return Group Object
     */
    public Group_Permissions getAccountGroup() {
        return accountGroup;
    }

    /**
     * Sets param without any inspection
     *
     * @param groupPermissions
     */
    public void setAccountGroup(Group_Permissions groupPermissions) {
        this.accountGroup = groupPermissions;
    }

    /**
     * Sets param without any inspection
     *
     * @return
     */
    public Set<Permission> getPermissions() {
        return permissions;
    }

    /**
     * Sets param without any inspection
     *
     * @param permissions
     */
    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    /**
     * Sets param without any inspection
     *
     * @param username The users username
     */
    public void setUsername(String username) {
        this.username = username;
    }


    /**
     * Sets param without any inspection
     *
     * @param password The usernames password
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
