package com.wipwebshop.backend.account.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <p>
 * Group_Permission represents a group, which can give members permission
 * </p>
 * <p>
 * Each group is just described by id and name
 * </p>
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "group_permissions")
public class Group_Permissions {
    @Id
    @Column(name = "groupid")
    private int groupId;
    @Column(name = "name")
    private String groupName;

    /**
     * Creates an empty Group_Permission Object
     */
    public Group_Permissions() {
    }

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Set @param without any inspection
     *
     * @param groupName
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * Returns @return without any inspection
     *
     * @return
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * Set @param without any inspection
     *
     * @param groupId
     */
    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
