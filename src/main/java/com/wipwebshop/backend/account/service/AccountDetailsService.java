package com.wipwebshop.backend.account.service;

import com.wipwebshop.backend.account.model.Account;
import com.wipwebshop.backend.account.model.AccountPrincipal;
import com.wipwebshop.backend.account.repository.AccountRepository;
import com.wipwebshop.backend.exceptions.UserAlreadyExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * This class implements services needed for actions belonging accounts.
 * </p>
 * <p>
 * Accounts could be created, found or modified.
 * </p>
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Service
public class AccountDetailsService implements UserDetailsService {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    final static int groupCustomer = 2;

    public AccountDetailsService() {
        super();
    }

    /**
     * Loads a user by username and returns as a AccountPrincipal Object
     *
     * @param username The username to search, not null
     * @return Found account as AccountPrincipal Object; Null if no account was found
     */
    @Override
    public UserDetails loadUserByUsername(final String username) {
        final Account account = accountRepository.findByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new AccountPrincipal(account);
    }

    /**
     * Loads all Users
     *
     * @return List of all accounts
     */
    public <Iterable> List<Account> loadAllUser() {
        return accountRepository.findAll();
    }

    /**
     * Adds new account, if username, mail do not exist already
     *
     * @param account New Account, not null
     */
    public void addNewAccount(Account account) {
        // check if username exists
        if (usernameExists(account.getUsername()) == true) {
            throw new UserAlreadyExistException(
                    "There is an account with that username:" + account.getUsername());
        }

        // check if email exists
        if (emailExists(account.getEmail()) == true) {
            throw new UserAlreadyExistException(
                    "There is an account with that email address:" + account.getEmail());
        }

        String username = account.getUsername();
        // db field must have a length of 60
        String password = passwordEncoder.encode(account.getPassword());

        String email = account.getEmail();
        String forename = account.getForename();
        String surname = account.getSurname();
        String street = account.getStreet();
        String housenumber = account.getHousenumber();
        String city = account.getCity();
        int postcode = account.getPostcode();
        String phone = account.getPhone();

        int groupId = account.getGroupId();

        // per default set role 2 for customer
        if (groupId == 0) {
            groupId = groupCustomer;
        }

        Account newAccount = new Account(username, password, email, forename, surname,
                street, housenumber, postcode, city, phone, groupId);
        accountRepository.save(newAccount);
    }

    /**
     * Checks if given username already exists
     *
     * @param username username to check
     * @return true, if username exists; false, is username not exists
     */
    private boolean usernameExists(String username) {
        return accountRepository.existsByUsername(username);
    }

    /**
     * Checks if given mail already exists
     *
     * @param email address to check
     * @return true, if mail exists; false, if mail not exists
     */
    private boolean emailExists(String email) {
        return accountRepository.existsByEmail(email);
    }

    /**
     * Loads a user by username and returns as a Account Object
     *
     * @param username The username to search, not null
     * @return Found account as Account Object; null if no account was found
     */
    public Account loadUserDetailsByUsername(final String username) {
        final Account account = accountRepository.findByUsername(username);
        return account;
    }

    /**
     * Modifies an existing account
     *
     * @param account        The new account object. Has to contain an existing accountId , not null
     * @param changePassword True if password should be changed.
     */
    public void changeAccount(Account account, boolean changePassword) {
        if (changePassword) {
            // db field must have a length of 60
            account.setPassword(passwordEncoder.encode(account.getPassword()));
        }

        // change changes to database - save can be used for update too
        accountRepository.save(account);
    }
}
