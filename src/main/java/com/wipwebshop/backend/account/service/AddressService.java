package com.wipwebshop.backend.account.service;

import com.wipwebshop.backend.account.model.Address;
import com.wipwebshop.backend.account.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * This Serviceclass implements methods beloging to addresses.
 * </p>
 *
 * @author Jonas Mertens
 */
@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    /**
     * Finds an equal addres, if one exists.
     *
     * @param address Address to look for equal
     * @return Found equal address; null if no equal address was found
     */
    private Address findEqual(Address address) {
        return addressRepository.findEqualAddress(address.getForename(), address.getSurname(), address.getStreet(), address.getHousenumber(), address.getPostcode(), address.getCity());
    }

    /**
     * Adds a new address, if it not already exists
     *
     * @param address Address to add
     * @return AddressId of new Address or AddressId or existing address
     */
    public int addAddress(Address address) {
        // check if username exists
        if (findEqual(address) != null) {
            return findEqual(address).getAddressId();
        } else {
            return addressRepository.save(address).getAddressId();
        }
    }

    /**
     * Finds Address by AddressId
     *
     * @param id AddressId to look for
     * @return Found Address fo Id
     */
    public Address findById(int id) {
        return addressRepository.findById(id).orElse(null);
    }
}
