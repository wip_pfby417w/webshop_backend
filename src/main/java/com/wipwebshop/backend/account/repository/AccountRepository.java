package com.wipwebshop.backend.account.repository;

import com.wipwebshop.backend.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository Class to access account data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    /**
     * Finds one and only one account by the username
     *
     * @param username The users username, not null
     * @return username as String
     */
    @Query(value = "SELECT username FROM account WHERE username =:username", nativeQuery = true)
    String findAccountByUsername(@Param("username") String username);

    /**
     * Finds one and only one account by the username
     *
     * @param username The users username, not null
     * @return user as AccountObject
     */
    Account findByUsername(String username);

    /**
     * Checks if the given mail already exists in the database.
     *
     * @param email The address to check, not null
     * @return true if exists, false is not exists
     */
    boolean existsByEmail(String email);

    /**
     * Checks if the given username already exists in the database.
     *
     * @param username The username to check, not null
     * @return true if exists, false is not exists
     */
    boolean existsByUsername(String username);
}

