package com.wipwebshop.backend.account.repository;


import com.wipwebshop.backend.account.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Repository Class to access address data in database. Methods such as save, select are available as default
 *
 * @author Jonas Mertens
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> {

    /**
     * Finds Adress in database, which is equal to given address to avoid duplicate data.
     *
     * @param forename    The forename to check, not null
     * @param surname     The surname to check, not null
     * @param street      The street to check, not null
     * @param housenumber The housenumber to check, not null
     * @param postcode    The postcode to check, not null
     * @param city        The city to check, not null
     * @return Address Object for equal address
     */
    @Query(value = "SELECT * FROM addresses WHERE forename =:forename AND surname=:surname AND street=:street AND housenumber=:housenumber AND postcode=:postcode AND city=:city ", nativeQuery = true)
    Address findEqualAddress(
            @Param("forename") String forename,
            @Param("surname") String surname,
            @Param("street") String street,
            @Param("housenumber") String housenumber,
            @Param("postcode") String postcode,
            @Param("city") String city);

}
