package com.wipwebshop.backend.account.repository;

import com.wipwebshop.backend.account.model.Group_Permissions;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository Class to access address data in database. Methods such as save, select are available as default.
 *
 * @author Rene Schiffner
 */
public interface PermissionRepository extends JpaRepository<Group_Permissions, Integer> {

}
