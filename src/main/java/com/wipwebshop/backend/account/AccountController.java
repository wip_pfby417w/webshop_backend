package com.wipwebshop.backend.account;

import com.wipwebshop.backend.account.model.Account;
import com.wipwebshop.backend.account.repository.AccountRepository;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Controllerclass for HTTP controllers belonging to accounts
 *
 * @author Rene Schiffner
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class AccountController {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    AccountDetailsService accountDetailsService;

    /**
     * account list for admin view
     */
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/admin/getAccounts")
    public @ResponseBody
    Iterable<Account> listAllAccounts() {
        return accountDetailsService.loadAllUser();
    }

    /**
     * Gets account for username
     *
     * @param username the username to look for. Extracted as {account} from url
     * @return account object as json, null if no account was found
     */
    @GetMapping(path = "/user/{account}")
    public @ResponseBody
    UserDetails getAccount(@PathVariable("account") String username) {
        return accountDetailsService.loadUserByUsername(username);
    }

    /**
     * Gets userdetails for username
     *
     * @param username the username to look for. Extracted from body as account
     * @return userDetails Object for found User
     * @throws IntrospectionException    //TODO
     * @throws InvocationTargetException //TODO
     * @throws IllegalAccessException    //TODO
     */
    @GetMapping(path = "/user/details/")
    public @ResponseBody
    HashMap<String, Object> showUserDetails(@RequestParam("account") String username)
            throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        Account account = accountDetailsService.loadUserDetailsByUsername(username);
        HashMap<String, Object> userDetails = new HashMap<>();
        //TODO Implementation of Exceptions

        // return details of an user
        BeanInfo beanInfo = Introspector.getBeanInfo(Account.class);
        for (PropertyDescriptor propertyDesc : beanInfo.getPropertyDescriptors()) {
            String propertyName = propertyDesc.getName();
            Object value = propertyDesc.getReadMethod().invoke(account);
            if (!propertyName.equals("password") && !propertyName.equals("class")) {
                userDetails.put(propertyName, value);
            }
        }
        return userDetails;
    }


    /**
     * Controller for Changing a AccountObject
     *
     * @param data Account Object with data to chage. Has to contain valid AccountId.
     * @throws BadCredentialsException   //TODO
     * @throws IntrospectionException    //TODO
     * @throws InvocationTargetException //TODO
     * @throws IllegalAccessException    //TODO
     */
    @PostMapping(path = "/user/change/", consumes = MediaType.ALL_VALUE)
    public void changeUserDetails(@RequestBody Account data)
            throws BadCredentialsException, IntrospectionException, InvocationTargetException, IllegalAccessException {

        boolean changePassword = false;

        // username is needed to load the account from database
        if (data.getUsername() == null || data.getPassword() == null || data.getEmail() == null) {
            throw new BadCredentialsException("Bad credentials");
        }
        // get account from database
        Account account = accountDetailsService.loadUserDetailsByUsername(data.getUsername());

        Class accountClass = Account.class;
        for (Method accountMethod : accountClass.getMethods()){
            // set name of getMethod
            String methodName = accountMethod.getName();

            // skip some methods
            if (methodName.equals("setAccountId") || methodName.equals("getAccountId") ||
                    methodName.equals("setUsername") || methodName.equals("getUsername") ){
                continue;
            }

            // loop only over getter
            if (methodName.startsWith("get")) {
                // set name of the setMethod
                String setMethodName = "set" + methodName.substring(3);
                // we need to get the current value and cast it to an object
                Object currentVal = accountMethod.invoke(data);

                // check if current method is password method and password is not set
                if (methodName.equals("getPassword")){
                    if(!data.getPassword().equals("placeholder")){
                        try{
                            // try to change the current password
                            Method setMethod = accountClass.getMethod(setMethodName, currentVal.getClass());
                            setMethod.invoke(account, accountMethod.invoke(data).toString());
                            changePassword = true;
                        }catch (NoSuchMethodException exception){
                            // TODO do nothing
                        }
                    }
                    continue;
                }

                try{
                    // try to change the current value of attribute
                    if(currentVal != null){
                        Method setMethod = accountClass.getMethod(setMethodName, currentVal.getClass());
                        setMethod.invoke(account, accountMethod.invoke(data));
                    }
                }catch (NoSuchMethodException exception) {
                    // TODO do nothing
                }
                System.out.println(accountMethod.invoke(account));
            }
        }

        // call account service to change account
        accountDetailsService.changeAccount(account, changePassword);
    }


    /**
     * Controller for registration of user. The user is added with role customer.
     *
     * @param account The new account.
     * @throws BadCredentialsException Thrown if username or password is null.
     */
    @PostMapping(path = "/user/registration")
    public void register(@RequestBody Account account) throws BadCredentialsException {
        if (account == null) {
            throw new BadCredentialsException("Bad credentials");
        }
        accountDetailsService.addNewAccount(account);
    }

}
