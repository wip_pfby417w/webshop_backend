package com.wipwebshop.backend.config.service;

import com.wipwebshop.backend.config.model.Config;
import com.wipwebshop.backend.config.repository.ConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * This Serviceclass implements methods belonging to configurations.
 * </p>
 *
 * @author Rene Schiffner
 */
@Service
public class ConfigService {

    @Autowired
    ConfigRepository configRepository;

    /**
     * Lists all active Configurations
     *
     * @return List of acitve configurtion parameters
     */
    public <Iterable> List<Config> loadActiveConfig() {
        List<Config> activeConfig = configRepository.findActive();
        return activeConfig;
    }

    /**
     * Loads one configration parameter by its name
     *
     * @param name Name of configuration to look for
     * @return Config object, if config was found. Else null
     */
    public Config loadConfigByName(String name) {
        return configRepository.findByConfigName(name);
    }

    /**
     * Edits an existing configuration.
     *
     * @param config New Config Object, ID must exist in database
     */
    public void editConfig(Config config) {
        // add new config option
        if (configRepository.findById(config.getConfigId()) == null) {
            Config newConfig = new Config();
            newConfig.setConfigName(config.getConfigName());
            newConfig.setConfigDesc(config.getConfigDesc());
            newConfig.setConfigValue(config.getConfigValue());
            newConfig.setActive(config.isActive());
            configRepository.save(newConfig);
        } else {
            Config savedConfig = configRepository.findById(config.getConfigId());
            savedConfig.setConfigName(config.getConfigName());
            savedConfig.setConfigDesc(config.getConfigDesc());
            savedConfig.setConfigValue(config.getConfigValue());
            savedConfig.setActive(config.isActive());
            configRepository.save(savedConfig);
        }
    }
}
