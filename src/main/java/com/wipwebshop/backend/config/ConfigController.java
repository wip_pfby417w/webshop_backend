package com.wipwebshop.backend.config;

import com.wipwebshop.backend.config.model.Config;
import com.wipwebshop.backend.config.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Controllerclass for HTTP controllers belonging to configs
 *
 * @author Rene Schiffner
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class ConfigController {

    @Autowired
    ConfigService configService;

    /**
     * Lists all active Configurations
     *
     * @return List of acitve configurations
     */
    @GetMapping(path = "/config/list")
    public @ResponseBody
    Iterable<Config> listActiveConfig() {
        return configService.loadActiveConfig();
    }


    /**
     * Edits an existing configuration
     *
     * @param config Config object with existing id and new parameters
     * @return OK if succes
     * @throws HttpClientErrorException.UnprocessableEntity Throws if configobject is null or invalid
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(path = "/config")
    public HttpStatus editConfig(@RequestBody Config config) throws HttpClientErrorException.UnprocessableEntity {
        if (config == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "config is null", null, null, null);
            throw http;
        }
        configService.editConfig(config);
        return HttpStatus.OK;
    }

}
