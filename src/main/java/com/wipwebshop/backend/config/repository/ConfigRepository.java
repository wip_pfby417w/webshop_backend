package com.wipwebshop.backend.config.repository;

import com.wipwebshop.backend.config.model.Config;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Repository Class to access config data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
public interface ConfigRepository extends JpaRepository<Config, Integer> {

    @Query(value = "SELECT * FROM config WHERE active", nativeQuery = true)
    <Iterable> List<Config> findActive();


    @Query(value = "SELECT * FROM config WHERE configName=:name", nativeQuery = true)
    Config findByConfigName(String name);

    Config findById(int configId);
}
