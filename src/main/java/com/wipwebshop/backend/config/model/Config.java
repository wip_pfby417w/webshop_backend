package com.wipwebshop.backend.config.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Model for persistens shop configuration
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "config")
public class Config {

    // primary key
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "configid")
    private int configId;

    @Column(name = "configname")
    private String configName;

    @Column(name="configdesc")
    private String configDesc;

    @NotNull
    @Column(name = "configvalue")
    private String configValue;

    @Column(name = "active")
    private boolean active;

    public int getConfigId() {
        return configId;
    }

    /**
     * Creates an empty config Object
     */
    public Config() {

    }

    /**
     * Sets configId without any inspection
     *
     * @param configId configId to set
     */
    public void setConfigId(int configId) {
        this.configId = configId;
    }

    /**
     * Returns configName without any inspection
     *
     * @return configName
     */
    public String getConfigName() {
        return configName;
    }

    /**
     * Sets configName without any inspection
     *
     * @param configName configName to set
     */
    public void setConfigName(String configName) {
        this.configName = configName;
    }

    /**
     * Returns description of configuration without any inspection
     *
     * @return Description of configuration
     */
    public String getConfigDesc() {
        return configDesc;
    }

    /**
     * Sets Description of configuration parameter without any inspection
     *
     * @param configDesc configuration descirption to set
     */
    public void setConfigDesc(String configDesc) {
        this.configDesc = configDesc;
    }

    /**
     * Gets the value of the configuration parameter without any inspection
     *
     * @return value of configuration parameter
     */
    public String getConfigValue() {
        return configValue;
    }

    /**
     * Sets the value of the configuration parameter without any inspection
     *
     * @param configValue New value of configuration parameter
     */
    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    /**
     * Returns the active state of configuration parameter
     *
     * @return whether configuration parameter is active or not
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the active state of configuration parameter
     *
     * @param active active state to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }
}
