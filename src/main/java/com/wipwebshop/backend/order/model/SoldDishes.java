package com.wipwebshop.backend.order.model;

/**
 * model of SoldDishes used for DailyClosing
 *
 * @author Rene Schiffner
 */
public class SoldDishes {

    private int dishId;

    private String dishName;

    private int amount;

    /**
     * Constructor to create an empty soldDishes object
     */
    public SoldDishes() {
    }

    /**
     * Constructor to create an totally filled soldDishes object
     *
     * @param dishId   dishId which was sold
     * @param dishName dishName which was sold
     * @param amount   amount of sold dish
     */
    public SoldDishes(int dishId, String dishName, int amount) {
        this.dishId = dishId;
        this.dishName = dishName;
        this.amount = amount;
    }

    /**
     * Returns the dishId of the soldDish
     *
     * @return dishId of soldDish
     */
    public int getDishId() {
        return dishId;
    }

    /**
     * sets the dishId
     *
     * @param dishId dishId to set
     */
    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    /**
     * returns the dishName of the soldDish
     *
     * @return dishName of the soldDish
     */
    public String getDishName() {
        return dishName;
    }

    /**
     * sets the dishName
     *
     * @param dishName dishName to set
     */
    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    /**
     * returns the amount of the soldDish
     *
     * @return amount of soldDish
     */
    public int getAmount() {
        return amount;
    }

    /**
     * sets the amount of the soldDish
     *
     * @param amount amount to set
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
