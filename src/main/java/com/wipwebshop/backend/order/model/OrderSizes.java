package com.wipwebshop.backend.order.model;

import com.wipwebshop.backend.dish.model.Size;

import javax.persistence.*;

/**
 * Model of ordered sizes
 * Represents the ordered size of an dish
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Entity
@Table(name = "order_sizes")
public class OrderSizes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ordersizeid")
    private int orderSizeId;

    @Column(name = "orderid")
    private int orderId;

    @Column(name = "sizeid")
    private int sizeId;

    @Column(name = "amount")
    private int amount;

    @Column(name = "priceperitem")
    private float pricePerItem;

    @Transient
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "sizeid")
    private Size size;

    /**
     * Constructor to create an empty OrderSize
     */
    public OrderSizes() {
    }

    /**
     * returns the orderId of an OrderSize
     *
     * @return orderId of belonging order
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * sets the belonging orderId
     *
     * @param orderId orderId to set
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * returns the sizeId of an OrderSize
     *
     * @return sizeId of belonging size
     */
    public int getSizeId() {
        return sizeId;
    }

    /**
     * sets the belonigng sizeId
     *
     * @param sizeId sizeId to set
     */
    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    /**
     * returns amount of ordered OrderSize
     *
     * @return amount of ordered OrderSize
     */
    public int getAmount() {
        return amount;
    }

    /**
     * sets amount of ordered OrderSize
     *
     * @param amount amount of ordered OrderSize
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Returns the price for which the OrderSize was sold
     *
     * @return price for OrderSize
     */
    public float getPricePerItem() {
        return pricePerItem;
    }

    /**
     * sets price for which the OrderSize was sold
     *
     * @param pricePerItem price to set
     */
    public void setPricePerItem(float pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

    /**
     * Returns the belonigng SizeObject
     *
     * @return SizeObject for belonging Size
     */
    public Size getSize() {
        return size;
    }

    /**
     * Sets the SizeObject for belonging Size Object
     *
     * @param size Size to set
     */
    public void setSize(Size size) {
        this.size = size;
    }

}

