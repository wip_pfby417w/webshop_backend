package com.wipwebshop.backend.order.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Model of daily_closing
 *
 * @author Rene Schiffner
 */
@Entity
@Table(name = "daily_closing")
public class DailyClosing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "closingid")
    private int closingId;

    @Column(name = "closingdate")
    private String closingDate;

    @Column(name = "ordercount")
    private int orderCount;

    @Column(name = "dishcount")
    private int dishCount;

    @Column(name = "earning")
    private double earning;

    @Column(name = "mwst")
    private double mwst;

    @Transient
    private List<SoldDishes> soldDishes = new ArrayList<SoldDishes>();

    public DailyClosing() {

    }

    /**
     * Returns the closingId without any inspection
     *
     * @return ClosingId
     */
    public int getClosingId() {
        return closingId;
    }

    /**
     * Sets the closing ID without any inspection
     *
     * @param closingId ClosingID to set
     */
    public void setClosingId(int closingId) {
        this.closingId = closingId;
    }

    /**
     * Returns the closingDate without any inspectino
     *
     * @return Date for dailyclosing
     */
    public String getClosingDate() {
        return closingDate;
    }

    /**
     * Sets the date of the dailyClosing
     *
     * @param closingDate closingDate to set
     */
    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    /**
     * Returns the count of orders in the dailyClosing
     *
     * @return count of orders
     */
    public int getOrderCount() {
        return orderCount;
    }

    /**
     * Sets the count of orders in the dailyClosing
     *
     * @param orderCount count to set
     */
    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    /**
     * Returns the earnings of the day
     *
     * @return earnings of the day
     */
    public double getEarning() {
        return earning;
    }

    /**
     * Sets the earnings of the day
     *
     * @param earning earnings to set
     */
    public void setEarning(double earning) {
        this.earning = earning;
    }

    /**
     * returns the count of sold dishes of the day
     *
     * @return count of sold dishes of the day
     */
    public int getDishCount() {
        return dishCount;
    }

    /**
     * Sets the count of sold dishes of the day
     *
     * @param dishCount cont of dishes to set
     */
    public void setDishCount(int dishCount) {
        this.dishCount = dishCount;
    }

    /**
     * returns the Mwst of the day
     *
     * @return mwst of the day
     */
    public double getMwst() {
        return mwst;
    }

    /**
     * sets the mwst of the day
     *
     * @param mwst mwst to set
     */
    public void setMwst(double mwst) {
        this.mwst = mwst;
    }

    /**
     * return a list of all sold dishes of the day
     *
     * @return list of sold dishes
     */
    public List<SoldDishes> getSoldDishes() {
        return soldDishes;
    }

    /**
     * sets a list of all sold dishes of the day
     *
     * @param soldDishes list to set
     */
    public void setSoldDishes(List<SoldDishes> soldDishes) {
        this.soldDishes = soldDishes;
    }
}
