package com.wipwebshop.backend.order.model;

import com.wipwebshop.backend.account.model.Address;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Model of orders
 * Each order can be places by an user
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Entity
@Table(name = "orders")
public class Order {

    // primary key in table
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderid")
    private int orderId;

    @NotNull
    @Column(name = "accountid")
    private int accountId;

    @Column(name = "date")
    private Timestamp date;

    @Column(name = "stateid")
    private int stateId;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "orderid")
    private List<OrderSizes> order_sizes = new ArrayList<>();

    @Column(name = "shippingaddress")
    private int shippingAddressId;

    @Column(name = "invoiceaddress")
    private int invoiceAddressId;

    @Transient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "addressid")
    private Address shippingAddress;

    @Transient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoiceaddress")
    private Address invoiceAddress;

    @Column(name = "paymentmethod")
    private int paymentMethod;

    @Column(name = "deliveryoption")
    private int deliveryOption;

    @Column(name = "mwst")
    private double mwst;

    /**
     * Constructor to create an empty order
     */
    public Order() {
    }

    /**
     * Constructor to create an order with the account, which places the order and when it was places
     *
     * @param accountId accountId, which placed the order
     * @param date      Date when the order was places
     */
    public Order(int accountId, Timestamp date) {
        this.accountId = accountId;
        this.date = date;
        // new order --> always state 1 for new order
        this.stateId = 1;
    }

    /**
     * Returns the accountId of the account, which places the order
     *
     * @return accountId which placed the order
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Sets the accountId which placed the order
     *
     * @param accountId accountId to set
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    /**
     * Returns the Id of the Order
     *
     * @return Id of the order
     */
    public int getOrderId() {
        return orderId;
    }

    /**
     * Sets the id of the order
     *
     * @param orderId id to set
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    /**
     * returns the date, when the order was places
     *
     * @return order placed date
     */
    public Timestamp getDate() {
        return date;
    }

    /**
     * sets the date, when the order was places
     *
     * @param date date to set
     */
    public void setDate(Timestamp date) {
        this.date = date;
    }

    /**
     * Returns the stateId of the order
     *
     * @return stateId of the order
     */
    public int getStateId() {
        return stateId;
    }

    /**
     * Sets the state id of the order
     *
     * @param stateId stateId to set
     */
    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    /**
     * Returns orders sizes
     *
     * @return List of ordered sizes
     */
    public List<OrderSizes> getOrder_sizes() {
        return order_sizes;
    }

    /**
     * Sets ordered sizes
     *
     * @param order_sizes List of ordered sizes
     */
    public void setOrder_sizes(List<OrderSizes> order_sizes) {
        this.order_sizes = order_sizes;
    }

    /**
     * Returns the shippingAddress of the order
     *
     * @return shippingAddress
     */
    public Address getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the shippingAddress of the order
     *
     * @param shippingAddress address to set
     */
    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * Returns the invoiceAddress of the order
     *
     * @return invoiceAddress
     */
    public Address getInvoiceAddress() {
        return invoiceAddress;
    }

    /**
     * Sets the invoiceAddress of the order
     *
     * @param invoiceAddress address to set
     */
    public void setInvoiceAddress(Address invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    /**
     * Returns the Id of the shipping Address
     *
     * @return Id of the shipping address
     */
    public int getShippingAddressId() {
        return shippingAddressId;
    }

    /**
     * Sets the id of the shippingAddress
     *
     * @param shippingAddressId shippingAddressId to set
     */
    public void setShippingAddressId(int shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    /**
     * Returns the Id of the invoice Address
     *
     * @return Id of the invoice address
     */
    public int getInvoiceAddressId() {
        return invoiceAddressId;
    }

    /**
     * Sets the id of the invoiceAddress
     *
     * @param invoiceAddressId invoiceAddressId to set
     */
    public void setInvoiceAddressId(int invoiceAddressId) {
        this.invoiceAddressId = invoiceAddressId;
    }

    /**
     * returns the paymentMethod used for the order
     *
     * @return paymentMethos as integer
     */
    public int getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * sets the paymentMethod for an order
     *
     * @param paymentMethod paymentMethod to set
     */
    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * returns the deliveryOption used for an order
     *
     * @return deliveryOption ad integer
     */
    public int getDeliveryOption() {
        return deliveryOption;
    }

    /**
     * sets the deliveryOption for an order
     *
     * @param deliveryOption deliveryOption to set
     */
    public void setDeliveryOption(int deliveryOption) {
        this.deliveryOption = deliveryOption;
    }

    /**
     * returns the mwst for an order
     *
     * @return mwst
     */
    public double getMwst() {
        return mwst;
    }

    /**
     * sets the mwst for an order
     *
     * @param mwst
     */
    public void setMwst(double mwst) {
        this.mwst = mwst;
    }
}
