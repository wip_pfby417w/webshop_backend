package com.wipwebshop.backend.order.service;

import com.wipwebshop.backend.account.service.AddressService;
import com.wipwebshop.backend.dish.service.SizeService;
import com.wipwebshop.backend.order.model.Order;
import com.wipwebshop.backend.order.model.OrderSizes;
import com.wipwebshop.backend.order.repository.OrderRepository;
import com.wipwebshop.backend.order.repository.OrderSizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * This Serviceclass implements methods belonging to orders.
 * </p>
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderSizeRepository orderSizeRepository;

    @Autowired
    AddressService addressService;

    @Autowired
    SizeService sizeService;

    /**
     * Gets all Orders
     *
     * @return List of all Orders
     */
    public <Iterable> List<Order> loadAllOrders() {
        return addTransientFields(orderRepository.findAll());
    }

    /**
     * Gets all orders in state new
     *
     * @return List of all orders in state new
     */
    public <Iterable> List<Order> loadAllNewOrders() {
        return addTransientFields(orderRepository.findOrdersByState(1));
    }

    /**
     * Finds one order by its Id
     *
     * @param orderId orderId to look for
     * @return found order, null if no order found
     */
    public Order loadOrderById(int orderId) {
        return addTransientFields(orderRepository.getOne(orderId));
    }

    /**
     * Loads all orders by given state
     *
     * @param stateId stateId to look for
     * @return List of found Orders
     */
    public <Iterable> List<Order> loadAllOrdersByState(int stateId) {
        return addTransientFields(orderRepository.findOrdersByState(stateId));
    }

    /**
     * Loads all orders between to dates
     *
     * @param dayBegin startdate
     * @param dayEnd   enddate
     * @return List of found orders
     */
    public <Iterable> List<Order> loadAllOrdersByDate(String dayBegin, String dayEnd) {
        return addTransientFields(orderRepository.findOrdersByDate(dayBegin, dayEnd));
    }


    /**
     * Loads all orders for an specific user
     *
     * @param accountId accountId to look for orders
     * @return List of found orders
     */
    public <Iterable> List<Order> loadAllOrdersByUser(int accountId) {
        return addTransientFields(orderRepository.findOrdersByUser(accountId));
    }


    /**
     * Loads all Sizes stored in the order
     *
     * @param orderId orderId to look for
     * @return List of Found OrderSizes
     */
    public <Iterable> List<OrderSizes> loadAllSizeByOrder(int orderId) {
        return addSizeTransientFields(orderSizeRepository.findOrdersByOrderId(orderId));
    }

    /**
     * Adds an order
     *
     * @param order order to add
     * @return Created orderId
     */
    public int addOrder(Order order) {
        // check if dish exists
        Order savedOrder = orderRepository.save(order);
        return savedOrder.getOrderId();
    }

    /**
     * Add an OrderSize to an existng Order
     *
     * @param size orderSize to add
     */
    public void addSizeToOrder(OrderSizes size) {
        OrderSizes savedSize = orderSizeRepository.save(size);
    }

    /**
     * Changes an existing order
     *
     * @param order order to change, has to contain valid orderId
     */
    public void changeOrder(Order order) {
        int orderId = order.getOrderId();
        int stateId = order.getStateId();
        orderRepository.setOrderStateId(orderId, stateId);
    }

    /**
     * sets transient fileds, as addresses to an List of order Objects
     *
     * @param orders List of order Objects
     * @return List of order Objects with filled transient fields
     */
    private <Iterable> List<Order> addTransientFields(List<Order> orders) {
        orders.forEach((order) -> {
            order.setInvoiceAddress(addressService.findById(order.getInvoiceAddressId()));
            order.setShippingAddress(addressService.findById(order.getShippingAddressId()));
            order.setOrder_sizes(addSizeTransientFields(order.getOrder_sizes()));
        });
        return orders;
    }

    /**
     * sets transient fields, as addresses in an Order Object
     *
     * @param order Order ot set fields
     * @return Order with transient fields set
     */
    private Order addTransientFields(Order order) {
        order.setInvoiceAddress(addressService.findById(order.getInvoiceAddressId()));
        order.setShippingAddress(addressService.findById(order.getShippingAddressId()));
        order.setOrder_sizes(addSizeTransientFields(order.getOrder_sizes()));
        return order;
    }

    /**
     * sets transient Filed in OrderSizes
     *
     * @param orderSizes orderSizes to set Fields
     * @return OrderSizes with transient fields set
     */
    private <Iterable> List<OrderSizes> addSizeTransientFields(List<OrderSizes> orderSizes) {
        orderSizes.forEach(orderSize -> {
            orderSize.setSize(sizeService.loadSizeById(orderSize.getSizeId()));
        });
        return orderSizes;
    }
}
