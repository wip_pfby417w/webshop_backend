package com.wipwebshop.backend.order.service;

import com.wipwebshop.backend.order.model.DailyClosing;
import com.wipwebshop.backend.order.repository.DailyClosingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * This Serviceclass implements methods belonging to DailyClosings.
 * </p>
 *
 * @author Rene Schiffner
 */
@Service
public class DailyClosingService {

    @Autowired
    DailyClosingRepository dailyClosingRepository;

    @Autowired
    OrderService orderService;

    /**
     * Finds Clsoing by date
     *
     * @param date date to find
     * @return found DailyClosing, null if not found
     */
    public DailyClosing findByDate(Date date) {
        return dailyClosingRepository.findDailyClosingByClosingDate(date);
    }

    /**
     * Finds Closing by ID
     *
     * @param closingId closingId to look for
     * @return found DailyClosing, null if not found
     */
    public DailyClosing findById(int closingId) {
        return dailyClosingRepository.getOne(closingId);
    }

    /**
     * Saves a new or edited dailyClosing
     *
     * @param closing closing to save
     * @return ID of saved closing
     */
    public int saveClosing(DailyClosing closing) {
        DailyClosing savedClosing = dailyClosingRepository.save(closing);
        return savedClosing.getClosingId();
    }

    /**
     * Deletes a existing DailyClosing
     *
     * @param closing closing to delete
     */
    public void deleteClosing(DailyClosing closing) {
        dailyClosingRepository.delete(closing);
    }
}
