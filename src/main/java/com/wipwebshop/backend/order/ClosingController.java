package com.wipwebshop.backend.order;

import com.wipwebshop.backend.config.model.Config;
import com.wipwebshop.backend.config.service.ConfigService;
import com.wipwebshop.backend.dish.model.Dish;
import com.wipwebshop.backend.dish.model.Size;
import com.wipwebshop.backend.dish.service.DishService;
import com.wipwebshop.backend.order.model.DailyClosing;
import com.wipwebshop.backend.order.model.Order;
import com.wipwebshop.backend.order.model.OrderSizes;
import com.wipwebshop.backend.order.model.SoldDishes;
import com.wipwebshop.backend.order.service.DailyClosingService;
import com.wipwebshop.backend.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Controllerclass for HTTP controllers belonging to DailyClosings
 *
 * @author Rene Schiffner
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class ClosingController {

    @Autowired
    DailyClosingService dailyClosingService;

    @Autowired
    OrderService orderService;

    @Autowired
    DishService dishService;

    @Autowired
    ConfigService configService;

    /**
     * Deletes an existing DailyClosing by date
     *
     * @param date date to delete closing
     * @return OK if deleted successfully
     * @throws HttpClientErrorException.UnprocessableEntity thrown if closing is not found
     */
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping(path = "/admin/closing/delete/")
    public HttpStatus deleteSize(@RequestBody String date) throws HttpClientErrorException.UnprocessableEntity {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date deleteDate;
        try {
            deleteDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            deleteDate = new Date();
        }

        // get closing from database
        DailyClosing dailyClosing = dailyClosingService.findByDate(deleteDate);
        if (dailyClosing == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST,
                    "Closing is null", null, null, null);
            throw http;
        }
        // delete closing
        dailyClosingService.deleteClosing(dailyClosing);
        return HttpStatus.OK;
    }

    /**
     * returns one dailyClosing by date
     *
     * @param dateString date to look for
     * @return daily closing by date
     */
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(path = "/admin/closing/")
    public @ResponseBody
    DailyClosing getDailyClosing(@RequestParam("date") String dateString) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date;

        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            date = new Date();
        }

        // return closing if exists
        DailyClosing dailyClosing = dailyClosingService.findByDate(date);

        // we need to format date to datetime string
        String strDayBegin = simpleDateFormat.format(date);

        String strDayEnd = strDayBegin + " 23:59:59";
        strDayBegin = strDayBegin + " 00:00:00";

        // get all orders for the current day
        List<Order> orders = orderService.loadAllOrdersByDate(strDayBegin, strDayEnd);
        int orderCount = 0;
        int dishCount = 0;
        float earnings = 0;

        List<SoldDishes> soldDishes = new ArrayList<SoldDishes>();
        SoldDishes soldDish = null;

        String closingDate = simpleDateFormat.format(date);
        Config configMwst = configService.loadConfigByName("mwst");
        double mwst = Double.valueOf(configMwst.getConfigValue());

        // no order found
        if (orders.isEmpty()){
            orderCount = 0;
            earnings = 0;
            dishCount = 0;
        }else {
            orderCount = orders.size();

            // loop through orders to get sum
            for (Order order : orders) {
                for (OrderSizes orderSize : order.getOrder_sizes()) {
                    boolean existsInList = false;
                    earnings = earnings + orderSize.getPricePerItem() * orderSize.getAmount();
                    dishCount = dishCount + orderSize.getAmount();

                    Size size = orderSize.getSize();
                    Dish dish = dishService.loadDishById(size.getDishId());

                    String dishName = dish.getDish() + " " + size.getSize();

                    // if list is empty add new entry
                    if (soldDishes.isEmpty()) {
                        soldDish = new SoldDishes(orderSize.getSizeId(), dishName, orderSize.getAmount());
                        soldDishes.add(soldDish);
                    } else {
                        // if dishId contains in list we need to add the amount
                        for (SoldDishes sold : soldDishes) {
                            if (sold.getDishId() == orderSize.getSizeId()) {
                                int amount = sold.getAmount() + orderSize.getAmount();
                                sold.setAmount(amount);
                                existsInList = true;
                            }
                        }

                        if (!existsInList) {
                            // list does not contain dishId  so we need to add a new entry to list
                            soldDish = new SoldDishes(orderSize.getSizeId(), dishName, orderSize.getAmount());
                            soldDishes.add(soldDish);
                        }
                    }
                    existsInList = false;
                    soldDish = null;
                }
            }
        }

        // if closing does not exist we need to create a new closing
        if (dailyClosing == null){
            // save closing
            dailyClosing = new DailyClosing();
            dailyClosing.setClosingDate(closingDate);
            dailyClosing.setOrderCount(orderCount);
            dailyClosing.setEarning(earnings);
            dailyClosing.setDishCount(dishCount);
            dailyClosing.setMwst(mwst);
            int closingId = dailyClosingService.saveClosing(dailyClosing);

            dailyClosing = dailyClosingService.findByDate(date);
        }

        dailyClosing.setSoldDishes(soldDishes);
        return dailyClosing;
    }
}
