package com.wipwebshop.backend.order.repository;


import com.wipwebshop.backend.order.model.DailyClosing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Repository Class to access DailyClosing data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
@Repository
public interface DailyClosingRepository extends JpaRepository<DailyClosing, Integer> {
    /**
     * Returns an existing dailyClosing for a specific date
     *
     * @param date Date to look for
     * @return all fields of daily closing
     */
    @Query(value = "SELECT * FROM daily_closing WHERE closingDate =:date", nativeQuery = true)
    DailyClosing findDailyClosingByClosingDate(@Param("date") Date date);
}
