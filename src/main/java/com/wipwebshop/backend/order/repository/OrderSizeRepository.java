package com.wipwebshop.backend.order.repository;

import com.wipwebshop.backend.order.model.OrderSizes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Repository Class to access OrderSize data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
public interface OrderSizeRepository extends JpaRepository<OrderSizes, Long> {

    /**
     * Returns an order by its id
     *
     * @param orderId orderId to find
     * @return Found order for given ID
     */
    @Query(value = "SELECT * FROM order_sizes WHERE orderid =:orderId", nativeQuery = true)
    List<OrderSizes> findOrdersByOrderId(@Param("orderId") int orderId);


}
