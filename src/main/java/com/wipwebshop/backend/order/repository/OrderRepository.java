package com.wipwebshop.backend.order.repository;

import com.wipwebshop.backend.order.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Repository Class to access Order data in database. Methods such as save, select are available as default
 *
 * @author Rene Schiffner
 */
@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {

    /**
     * Returns all order whitch a given state
     *
     * @param stateId state which order must have
     * @return List of orders in this state
     */
    @Query(value = "SELECT * FROM orders WHERE stateId =:stateId", nativeQuery = true)
    List<Order> findOrdersByState(@Param("stateId") int stateId);

    /**
     * Returns all orders for a given account
     *
     * @param accountId accountId to look for
     * @return List of Orders for specific account
     */
    @Query(value = "SELECT * FROM orders WHERE accountId =:accountId", nativeQuery = true)
    List<Order> findOrdersByUser(@Param("accountId") int accountId);

    /**
     * Returns orders between to dates
     *
     * @param dayBegin begin of searchdate
     * @param dayEnd   end of searchdate
     * @return List of found orders between two dates
     */
    @Query(value = "SELECT * FROM orders WHERE date BETWEEN :dayBegin AND :dayEnd", nativeQuery = true)
    List<Order> findOrdersByDate(@Param("dayBegin") String dayBegin, @Param("dayEnd") String dayEnd);

    /**
     * Sets state of a given order
     *
     * @param orderId orderId of order to edit
     * @param stateId stateId to set
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE orders o set o.stateId = :stateId WHERE o.orderId = :orderId", nativeQuery = true)
    void setOrderStateId(@Param("orderId") int orderId, @Param("stateId") int stateId);

}
