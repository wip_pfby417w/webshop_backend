package com.wipwebshop.backend.order;

import com.wipwebshop.backend.account.model.Account;
import com.wipwebshop.backend.account.service.AccountDetailsService;
import com.wipwebshop.backend.exceptions.NotExistException;
import com.wipwebshop.backend.order.model.Order;
import com.wipwebshop.backend.order.model.OrderSizes;
import com.wipwebshop.backend.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;

/**
 * Controllerclass for HTTP controllers belonging to orders
 *
 * @author Rene Schiffner, Jonas Mertens
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@Service
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    AccountDetailsService accountDetailsService;

    /**
     * Returns the orders for an user
     *
     * @param username username to look for orders
     * @return List of orders
     */
    @GetMapping(path = "/user/orders/")
    public @ResponseBody
    Iterable<Order> listOrdersForUser(@RequestParam("account") String username) {
        // we need to get the account id to filter orders
        Account account = accountDetailsService.loadUserDetailsByUsername(username);
        List<Order> orders = orderService.loadAllOrdersByUser(account.getAccountId());
        return orders;
    }

    /**
     * Returns all OrderSizes for an Order
     *
     * @param orderId orderId to look for
     * @return OrderSIzes for given orderId
     */
    @GetMapping(path = "/user/orders/{orderId}")
    public @ResponseBody
    List<OrderSizes> loadSelectedDish(@PathVariable("orderId") int orderId) {
        // get all articles with size for order
        return orderService.loadAllSizeByOrder(orderId);
    }

    /**
     * Returns  order foung by id
     *
     * @param orderId orderId to look for
     * @return found order
     */
    @GetMapping(path = "/user/order/")
    public @ResponseBody
    Order loadSelectedOrder(@RequestParam("orderId") int orderId) {
        // get all articles with size for order
        return orderService.loadOrderById(orderId);
    }

    /**
     * Loads all orders
     *
     * @return List of all Orders
     * @throws NotExistException thrown if no order was found
     */
    @GetMapping(path = "/admin/orders/")
    public @ResponseBody
    Iterable<Order> listOrdersByState() throws NotExistException {
        List<Order> orders = orderService.loadAllOrders();
        if (orders.isEmpty()) {
            //throw new NotExistException("No order found");
        }
        return orders;
    }

    /**
     * adds a new order
     *
     * @param order order to add
     * @return id of created order
     * @throws HttpClientErrorException.UnprocessableEntity thrown if order is null
     */
    @PostMapping(path = "/order")
    public int addDish(@RequestBody Order order) throws HttpClientErrorException.UnprocessableEntity {
        if (order == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        return orderService.addOrder(order);
    }

    /**
     * Adds OrderSIzes (= ordered articles) to order
     *
     * @param size size to add to order
     * @throws HttpClientErrorException.UnprocessableEntity thrown, if size is null
     */
    @PostMapping(path = "/order/size")
    public void addOrderedSize(@RequestBody OrderSizes size) throws HttpClientErrorException.UnprocessableEntity {
        if (size == null) {
            HttpClientErrorException http = HttpClientErrorException.UnprocessableEntity.create(HttpStatus.BAD_REQUEST, "Item is null", null, null, null);
            throw http;
        }
        orderService.addSizeToOrder(size);
    }

    /**
     * edits an order starte
     *
     * @param order order to edit with new state
     */
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping(path = "/admin/order/state")
    public void setOrderState(@RequestBody Order order) {
        if (order == null) {

        }
        orderService.changeOrder(order);
    }

}



